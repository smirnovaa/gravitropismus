\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Gravitropismus als wichtige Pflanzeneigenschaft}{4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Fachliche Analyse der Thematik: Gravitropismus}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Gravitropismus}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Differenzielles Wachstum}{5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Koordination von Gravitropismus durch Pflanzenhormone}{8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Experimenteller Nachweis von Gravitropismus bei \emph {Lepidium sativum}}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Methoden}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Pflanzen}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Verwendete Materialien und Ger\IeC {\"a}te}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Klinostat}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.3}Versuchsbeschreibung}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Vorbereitung}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Durchf\IeC {\"u}hrung}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Ergebnisse}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Diskussion}{13}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Fazit und Ausblick}{14}
